import fire
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore

from imgcropper.app import MainWin


def main(**kwargs):
    app = pg.mkQApp()
    win = MainWin(**kwargs)
    QtCore.QTimer.singleShot(0, win.showMaximized)
    app.exec_()


if __name__ == "__main__":
    fire.Fire(main)
