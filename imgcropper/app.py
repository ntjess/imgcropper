# -*- coding: utf-8 -*-
import logging

import qtextras as qte
from pyqtgraph.Qt import QtCore, QtWidgets

from imgcropper.control import ImgCropperCtrl
from imgcropper.model import ImgCropperModel
from imgcropper.viewer import ImgCropperViewer


class MainWin(QtWidgets.QMainWindow):
    def __init__(
        self,
        dataFolder: str = "",
        outputFolder: str = ".",
        ext: str = "png",
        title=None,
        **kargs
    ):
        super().__init__(title, **kargs)
        self.viewer = ImgCropperViewer(self)
        self.model = ImgCropperModel()
        self.ctrl = ImgCropperCtrl(self.viewer, self.model)
        self.ctrl.inputData.setValue(dataFolder)
        self.ctrl.outputFolder.setValue(outputFolder)
        self.ctrl.imageExtension.setValue(ext)
        if len(dataFolder) > 0:
            self.ctrl.resetFiles()
            self.ctrl.changeCurrentImage()
        curImgLbl = QtWidgets.QLabel("No Image Selected", self)
        curImgLbl.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        updateLbl = lambda: curImgLbl.setText(
            self.ctrl.allFiles[self.ctrl.curFileIdx].name
        )
        self.ctrl.rootParameter.child("changeCurrentImage").sigActivated.connect(
            updateLbl
        )
        qte.EasyWidget.buildMainWindow(
            [[curImgLbl, self.viewer], self.ctrl],
            useSplitter=True,
            layout="H",
            win=self,
        )
        oldClass = logging.getLoggerClass()
        logging.setLoggerClass(qte.AppLogger)
        logger: qte.AppLogger = logging.getLogger(__name__)
        logging.setLoggerClass(oldClass)
        logger.registerExceptions(self)
        logger.setLevel(logging.DEBUG)
