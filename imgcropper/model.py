import typing as t
from functools import wraps
from pathlib import Path

import cv2 as cv
import numpy as np
from pyqtgraph.Qt import QtCore
from qtextras import ActionStack
from scipy.ndimage import binary_fill_holes
from skimage import img_as_float
from skimage import transform as trans

NChanImg = np.ndarray

stack = ActionStack(5)


def _imgChanger(descr: str = None):
    def wrapper(func):
        nonlocal descr
        if descr is None:
            descr = func.__name__

        @wraps(func)
        @stack.undoable(descr)
        def innerWrapper(*args, **kwargs):
            self = args[0]
            if self.img is None:
                oldImg = None
            else:
                oldImg = self.img.copy()
            func(*args, **kwargs)
            self.sigImageChanged.emit(self.img)
            yield self.img
            self.img = oldImg
            self.sigImageChanged.emit(self.img)

        return innerWrapper

    return wrapper


class ImgCropperModel(QtCore.QObject):
    sigImageChanged = QtCore.Signal(object)  # The newest image
    sigChangesApplied = QtCore.Signal()

    def __init__(self):
        super().__init__()
        self.img: t.Optional[NChanImg] = None
        self.origImg: t.Optional[NChanImg] = None

    @_imgChanger()
    def load(self, newImg: t.Union[NChanImg, Path, str]):
        if not isinstance(newImg, NChanImg) and newImg is not None:
            newImg = cv.imread(str(newImg))
            newImg = cv.cvtColor(newImg, cv.COLOR_BGR2RGB)
        self.img = newImg
        self.origImg = newImg.copy() if newImg is not None else newImg

    @_imgChanger()
    def reset(self):
        self.img = self.origImg.copy()

    @stack.undoable("Apply Changes")
    def applyChanges(self):
        oldOrig = self.origImg.copy()
        self.origImg = self.img.copy()
        self.sigChangesApplied.emit()
        yield
        self.origImg = oldOrig
        self.sigChangesApplied.emit()

    @_imgChanger()
    def rotate(self, angle=0.0):
        rot = trans.rotate(self.origImg, angle, resize=True, cval=0)
        self.img = (rot * 255).astype("uint8")

    @_imgChanger()
    def crop(self, cropRect: np.ndarray):
        """
        Crops the image
        :param cropRect: [[minRow, minCol], [maxRow, maxCol]].
        :return:
        """
        self.img = self.img[
            cropRect[0, 0] : cropRect[1, 0] + 1, cropRect[0, 1] : cropRect[1, 1] + 1
        ]

    def getAutoCropBounds(self, thresh=200, applyCrop=False):
        """Assumes a white paper background. Flood fill from edges to remove thsi background"""
        fgComps = self.nonPaperMask(thresh)

        onPixs = np.nonzero(fgComps)
        onPixs = np.c_[onPixs]
        cropBounds = np.vstack(
            [onPixs.min(0, keepdims=True), onPixs.max(0, keepdims=True)]
        )
        if applyCrop:
            self.crop(cropBounds)
        return cropBounds

    def nonPaperMask(self, thresh=200):
        threshed = self.img > thresh
        if threshed.ndim > 2:
            threshed = np.bitwise_and.reduce(threshed, 2)

        nonPaperComps = binary_fill_holes(~threshed)
        return nonPaperComps

    def getAutoRotateAngle(self, thresh=200, thetaRes=0.01, downsampSz=500):
        """
        Get the angle that the foreground component should be rotated, assuming it has strong
        straight-line edges and is against a white background.
        :param thresh: Intensity value across all image channels to look for white. Higher
          means the background should be whiter
        :param testAngles_deg: Determines the resolution and angle space this algorithm searches
          through when looking for the rotation angles. If none is provided, a default space
          of np.linspace(-10,10,500) is used.
        :param applyRotation: If *True*, the extracted rotation angle will be applied to the
          model's image
        """
        maxDim = max(self.img.shape[:2])
        if maxDim > downsampSz:
            testImg = cv.resize(
                self.img, (0, 0), fx=downsampSz / maxDim, fy=downsampSz / maxDim
            )
        else:
            testImg = self.img.copy()
        testImg = cv.cvtColor(testImg, cv.COLOR_RGB2GRAY)
        # Square to increase contrast
        testImg = ((img_as_float(testImg) ** 2) * 255).astype("uint8")
        fgEdges = cv.Canny(testImg, 50, thresh)
        # Use edges of largest comp as hough guides
        lines = cv.HoughLinesP(fgEdges, 1, thetaRes, 10)
        linePts = np.vstack(lines)
        diffs = linePts[:, 2:] - linePts[:, :2]
        angles = np.arctan2(*diffs.T)
        scores = np.arange(len(lines), 0, -1) * np.maximum(
            np.cos(angles), np.sin(angles)
        )
        return angles[np.argmax(scores)]

    def _imgEdges(self):
        # Taken directly from https://docs.opencv.org/3.4/d5/db5/tutorial_laplace_operator.html
        src = cv.GaussianBlur(self.img, (3, 3), 0)
        src_gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
        sobX = cv.Sobel(src_gray, cv.CV_16S, 1, 0, ksize=7)
        sobY = cv.Sobel(src_gray, cv.CV_16S, 0, 1, ksize=7)
        mag = np.abs(sobX) + np.abs(sobY)
        return mag


# mod = ImgCropperModel()
# mod.resetImage('C:/Users/ntjes/Pictures/49.png')
# mod.getAutoRotateAngle()
