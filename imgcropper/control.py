import contextlib
from functools import wraps
from pathlib import Path
from typing import Any, List

import cv2 as cv

# Used in 'eval'
# noinspection PyUnresolvedReferences
import numpy as np
import pyqtgraph as pg
from pyqtgraph.parametertree import InteractiveFunction
from qtextras import OptionsDict, ParameterEditor, RunOptions

from imgcropper.model import ImgCropperModel, stack
from imgcropper.utils import crop, sort_preferIntName
from imgcropper.viewer import ImgCropperViewer


def _blockable(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]
        if not self._blocked:
            return func(*args, **kwargs)

    return wrapper


class ImgCropperCtrl(ParameterEditor):
    model: ImgCropperModel
    viewer: ImgCropperViewer

    rotationAngle_deg: float

    def __init__(self, viewer: Any, model: Any):
        self.moveRoi = InteractiveFunction(self.moveRoi)
        self.moveRoi.parametersNeedRunKwargs = True
        super().__init__()
        self.viewer = viewer
        self.model = model
        # Get handle to 'move roi' process for param access
        self._createPropRefs()

        self.allFiles: List[Path] = []
        self.curFileIdx = -1

        def testSetImg(img):
            self.viewer.setImage(img)

        self.model.sigImageChanged.connect(testSetImg)
        self.model.sigChangesApplied.connect(lambda: self.handleChangesApplied())
        self.roiVisuals()
        self.moveRoi()
        self._blocked = False

        self.registerFunction(stack.undo, runActionTemplate={"shortcut": "Ctrl+Z"})
        self.registerFunction(stack.redo, runActionTemplate={"shortcut": "Ctrl+Y"})
        self.registerFunction(self.resetFiles, runActionTemplate={"shortcut": "Ctrl+R"})
        self.registerFunction(
            self.changeCurrentImage, runActionTemplate={"shortcut": "Ctrl+N"}
        )
        self.registerFunction(self.roiVisuals, runOptions=RunOptions.ON_CHANGED)
        self.registerFunction(self.moveRoi)
        self.registerFunction(self.cropImage, runActionTemplate={"shortcut": "Ctrl+C"})
        self.registerFunction(self.saveImage, runActionTemplate={"shortcut": "Ctrl+S"})
        self.registerFunction(self.autoRotate)

        rotParam = self.rootParameter.child("moveRoi")

        def updateParams(roi: pg.ROI):
            rotParam["rotation"] = roi.angle()
            rotParam["height"] = roi.size()[1]
            rotParam["width"] = roi.size()[0]
            rotParam["xPos"] = roi.pos().x()
            rotParam["yPos"] = roi.pos().y()
            self.viewer.line.setAngle(rotParam["rotation"])

        self.viewer.roi.sigRegionChanged.connect(updateParams)

    def _createPropRefs(self):
        """Define class method here so PyCharm knows about member variables"""
        props = [
            OptionsDict("Input Data", ".", type="file", fileMode="Directory"),
            OptionsDict("Image Extension", "png", type="str"),
            OptionsDict("Output Folder", ".", type="file", fileMode="Directory"),
        ]
        (
            self.inputData,
            self.imageExtension,
            self.outputFolder,
        ) = self.registerParameterList(props, namePath=("Data Options",))

    def resetFiles(self):
        if len(self.inputData.value()) == 0:
            return
        inFolder = Path(self.inputData.value())
        ext = self.imageExtension.value()
        self.allFiles = list(inFolder.glob(f"*.{ext}"))
        self.allFiles.sort(key=sort_preferIntName)
        self.curFileIdx = -1

    def changeCurrentImage(self, incrAmt=1):
        self.curFileIdx += incrAmt
        try:
            inImgPath = self.allFiles[self.curFileIdx]
        except IndexError:
            return
        self.model.load(inImgPath)

    def autoRotate(self, thresh=200, thetaRes=0.01, downsampSz=500):
        with pg.BusyCursor():
            optimalAngle = self.model.getAutoRotateAngle(thresh, thetaRes, downsampSz)
            self.viewer.roi.setAngle(optimalAngle)

    def locatePcbRect(self, newThresh):
        bounds = self.model.getAutoCropBounds(newThresh)
        self.viewer.roi.setPos(bounds[0])
        self.viewer.roi.setSize((bounds[1] - bounds[0])[::-1])

    def roiVisuals(self, show=True, lineWidth=4, lineColor="#000"):
        """
        :param show:
        :param lineWidth:
        :param lineColor:
          pType: color
        """
        roi = self.viewer.roi
        roi.pen.setWidth(lineWidth)
        roi.pen.setColor(pg.mkColor(lineColor))
        roi.hoverPen.setWidth(lineWidth)
        roi.setVisible(show)

        for handle in roi.handles:
            handle["item"].pen.setWidth(lineWidth)

    def moveRoi(self, xPos=0.0, yPos=0.0, width=300.0, height=300.0, rotation=0.0):
        self.viewer.roi.setAngle(rotation)
        self.viewer.roi.setPos(xPos, yPos)
        self.viewer.roi.setSize((width, height))
        self.viewer.line.setAngle(rotation)

    @stack.undoable("Crop Image")
    def cropImage(self):
        oldImg = self.viewer.imgItem.image
        with pg.BusyCursor():
            roi = self.viewer.roi
            topLeft = (roi.pos().y(), roi.pos().x())
            size = (roi.size().x(), roi.size().y())
            saveImg = crop(self.viewer.imgItem.image, topLeft, size, roi.angle())
            # saveImg = self.viewer.roi.getArrayRegion(
            #   self.viewer.imgItem.image, self.viewer.imgItem).astype('uint8')
        self.viewer.imgItem.setImage(saveImg)
        roiParams = self.moveRoi.parameters
        oldProps = {k: v.value() for k, v in roiParams.items()}
        self.moveRoi(
            xPos=0,
            yPos=0,
            rotation=0.0,
            width=saveImg.shape[1],
            height=saveImg.shape[0],
        )
        self.model.img = saveImg
        yield
        self.viewer.imgItem.setImage(oldImg)
        self.model.img = oldImg
        self.moveRoi(**oldProps)
        self.viewer.plotItem.getViewBox().autoRange()

    def saveImage(self):
        fname = self.allFiles[self.curFileIdx].name
        outName = Path(self.outputFolder.value()) / fname
        with pg.BusyCursor():
            img = cv.cvtColor(self.viewer.imgItem.image, cv.COLOR_RGB2BGR)
            cv.imwrite(str(outName), img)

    @contextlib.contextmanager
    def block(self):
        self._blocked = True
        yield
        self._blocked = False
