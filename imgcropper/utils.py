import re
from pathlib import Path
from typing import Union
from warnings import warn

import cv2 as cv
import numpy as np


class ImgCropperWarning(UserWarning):
    pass


def crop(img: np.ndarray, topLeft_rc, size_wh, angle_deg=0.0):
    size_wh = np.array(size_wh).astype(int)
    size_rc = size_wh[::-1]
    topLeft_rc = np.array(topLeft_rc, dtype="float32")
    bounds = np.vstack([topLeft_rc, topLeft_rc + size_rc]).astype(int)
    topLeft_rc = np.clip(topLeft_rc, 0, img.shape[:2])

    # Convert to int
    # Get rotation matrix for rectangle
    M = cv.getRotationMatrix2D(tuple(topLeft_rc[::-1]), angle_deg, 1)
    # Perform rotation on src image
    # Worst-case scenario is 45-degree rotation
    dstSize = (np.array(img.shape[:2]) * np.sqrt(2)).astype(int)
    fillVal = int(img.max())
    if img.ndim > 2:
        fillVal = tuple([fillVal for _ in range(img.shape[2])])
    dst = cv.warpAffine(img, M, tuple(dstSize)[::-1], borderValue=fillVal)

    bounds = np.clip(bounds, 0, dst.shape[:2])
    out = dst[bounds[0, 0] : bounds[1, 0], bounds[0, 1] : bounds[1, 1], :]
    if out.size == 0:
        warn(
            "Rect is too far outside the image area. No crop performed.",
            ImgCropperWarning,
        )
        return img
    return out


def sort_preferIntName(name: Union[str, Path]):
    name = str(name)
    return [int(x) if x.isdigit() else x for x in re.findall(r"[^0-9]|[0-9]+", name)]
