import pyqtgraph as pg

pg.setConfigOption("imageAxisOrder", "row-major")


class BigHandleROI(pg.RectROI):
    def __init__(self, *args, **kwargs):
        oldHandle = pg.RectROI.addScaleHandle
        pg.RectROI.addScaleHandle = lambda *_args: None
        super().__init__(*args, **kwargs)
        pg.RectROI.addScaleHandle = oldHandle

        # addHandleKeys = [k for k in pg.ROI.__dict__ if 'add' in k and 'handle' in k.lower()]
        addHandleKeys = "addScaleHandle", "addRotateHandle"
        for k in addHandleKeys:
            oldFn = getattr(self, k)

            def newFn(*args, _oldFn=oldFn, **kwargs):
                self.handleSize = 10
                self.handlePen = pg.mkPen("r")
                return _oldFn(*args, **kwargs)

            setattr(self, k, newFn)


class ImgCropperViewer(pg.PlotWidget):
    def __init__(self, parent=None, background="default", plotItem=None, **kargs):
        super().__init__(parent, background, plotItem, **kargs)
        self.imgItem = pg.ImageItem()
        self.imgItem.setBorder("r")
        self.roi = BigHandleROI([0, 0], [0, 0])
        self.roi.addRotateHandle([0.5, 0], [0, 0])
        self.roi.addRotateHandle([0, 0.5], [0, 1])
        self.roi.addRotateHandle([1, 0.5], [1, 0])
        self.roi.addRotateHandle([0.5, 1], [1, 1])
        self.roi.addScaleHandle([0, 0], [1, 1])
        self.roi.addScaleHandle([1, 0], [0, 1])
        self.roi.addScaleHandle([0, 1], [1, 0])
        self.roi.addScaleHandle([1, 1], [0, 0])

        self.line = pg.InfiniteLine(movable=True, angle=0)
        self.roi.hoverPen = self.line.hoverPen
        vb = self.getViewBox()
        vb.setAspectLocked(True)
        vb.invertY(True)

        self.setImage = self.imgItem.setImage

        for it in self.imgItem, self.roi, self.line:
            self.addItem(it)
        self.roi.hide()
        self.line.hide()
