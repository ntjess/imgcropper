# ImgCropper
## An image cropping (and rotating) python application

## Installation
```bash
git clone https://gitlab.com/ntjess/imgcropper
pip install -e ./imgcropper
```

## Usage
```bash
python -m imgcropper [--dataFolder </path/to/folder/with/images/>]
```